<?php

namespace Tests\Smorken\Data\Stubs;

use Smorken\Data\Data;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapInputName(SnakeCaseMapper::class)]
class UserViewData extends Data
{
    public function __construct(
        public ?int $id,
        public int $ownerId
    ) {
    }
}
