<?php

namespace Tests\Smorken\Data\Stubs;

use Smorken\Data\Data;
use Spatie\LaravelData\Lazy;

class DataWithLazy extends Data
{
    public function __construct(
        public ?int $id,
        public Lazy|string|null $lazy
    ) {
    }

    public static function fromId(int $id): self
    {
        return self::from([
            'id' => $id,
            'lazy' => Lazy::when(fn () => $id === 2, fn () => 'resolved'),
        ]);
    }
}
