<?php

namespace Tests\Smorken\Data\Stubs;

use Smorken\Data\Concerns\AuthorizesData;

class UserViewAuthorizesDataStub
{
    protected string $gateDataClass = UserViewData::class;

    use AuthorizesData;
}
