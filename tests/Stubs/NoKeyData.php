<?php

namespace Tests\Smorken\Data\Stubs;

use Smorken\Data\Data;

class NoKeyData extends Data
{
    public function __construct(public string $foo)
    {
    }
}
