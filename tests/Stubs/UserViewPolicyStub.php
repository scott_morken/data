<?php

namespace Tests\Smorken\Data\Stubs;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Data\Contracts\Data;
use Smorken\Data\DataGatePolicy;

class UserViewPolicyStub extends DataGatePolicy
{
    public function before(Authenticatable $user, $ability): ?bool
    {
        if ($user->getAuthIdentifier() === 99) {
            return true;
        }

        return null;
    }

    protected function isUserAllowedToView(Authenticatable $user, Data $data): Response
    {
        if ($user->getAuthIdentifier() === $data->ownerId) {
            return $this->allow();
        }

        return parent::isUserAllowedToView($user, $data);
    }
}
