<?php

namespace Tests\Smorken\Data\Stubs;

use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public function __construct(
        public int $id,
        public string $firstName = 'First',
        public string $lastName = 'Last'
    ) {
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthPassword()
    {
        return null;
    }

    public function getAuthPasswordName()
    {
        return 'password';
    }

    public function getRememberToken()
    {
        return null;
    }

    public function getRememberTokenName()
    {
        return 'token';
    }

    public function setRememberToken($value)
    {

    }
}
