<?php

namespace Tests\Smorken\Data\Stubs;

use Illuminate\Database\Eloquent\Model;
use Smorken\Data\Contracts\HasDataTransferObject;
use Spatie\LaravelData\WithData;

class ModelWithDTOStub extends Model implements HasDataTransferObject
{
    use WithData;

    protected string $dataClass = UserViewData::class;
}
