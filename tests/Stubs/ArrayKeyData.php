<?php

namespace Tests\Smorken\Data\Stubs;

use Smorken\Data\Data;

class ArrayKeyData extends Data
{
    public function __construct(public ?array $id)
    {
    }
}
