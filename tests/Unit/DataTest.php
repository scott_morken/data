<?php

namespace Tests\Smorken\Data\Unit;

use Tests\Smorken\Data\Stubs\ArrayKeyData;
use Tests\Smorken\Data\Stubs\NoKeyData;
use Tests\Smorken\Data\Stubs\UserViewData;
use Tests\Smorken\Data\TestCase;

class DataTest extends TestCase
{
    public function testArrayKeyWhenNull(): void
    {
        $sut = new ArrayKeyData(null);
        $this->assertTrue($sut->hasKey());
        $this->assertNull($sut->getKey());
    }

    public function testArrayKeyWhenSet(): void
    {
        $sut = new ArrayKeyData(['foo', 1]);
        $this->assertTrue($sut->hasKey());
        $this->assertEquals(['foo', 1], $sut->getKey());
    }

    public function testNoKeyOnData(): void
    {
        $sut = new NoKeyData('bar');
        $this->assertFalse($sut->hasKey());
        $this->assertFalse($sut->getKey());
    }

    public function testStringKeyWhenNull(): void
    {
        $sut = new UserViewData(null, 1);
        $this->assertTrue($sut->hasKey());
        $this->assertNull($sut->getKey());
    }

    public function testStringKeyWhenSet(): void
    {
        $sut = UserViewData::from(['id' => 1, 'owner_id' => 99]);
        $this->assertTrue($sut->hasKey());
        $this->assertEquals(1, $sut->getKey());
        $this->assertEquals(99, $sut->ownerId);
    }
}
