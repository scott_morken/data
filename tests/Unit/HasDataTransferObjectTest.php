<?php

namespace Tests\Smorken\Data\Unit;

use Tests\Smorken\Data\Stubs\ModelWithDTOStub;
use Tests\Smorken\Data\Stubs\UserViewData;
use Tests\Smorken\Data\TestCase;

class HasDataTransferObjectTest extends TestCase
{
    public function testToData(): void
    {
        $m = (new ModelWithDTOStub())->forceFill(['id' => 1, 'owner_id' => 99]);
        $sut = $m->getData();
        $this->assertInstanceOf(UserViewData::class, $sut);
        $this->assertEquals(['id' => 1, 'ownerId' => 99], $sut->toArray());
    }
}
