<?php

namespace Tests\Smorken\Data\Unit;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use PHPUnit\Framework\TestCase;
use Smorken\Data\Constants\GatePolicyType;
use Tests\Smorken\Data\Stubs\User;
use Tests\Smorken\Data\Stubs\UserViewAuthorizesDataStub;
use Tests\Smorken\Data\Stubs\UserViewData;
use Tests\Smorken\Data\Stubs\UserViewPolicyStub;

class DataGatePolicyTest extends TestCase
{
    public function testCanSkipAuthorizationUsingAuthorizeSelf(): void
    {
        $gate = $this->getGate(new User(2));
        UserViewAuthorizesDataStub::setGate($gate);
        UserViewData::setGate($gate);
        $data = new UserViewData(1, 10);
        $this->assertTrue($data->authorizeSelf(GatePolicyType::VIEW));
    }

    public function testDefineSelf(): void
    {
        $gate = $this->getGate(null);
        UserViewPolicyStub::defineSelf(UserViewData::class, $gate);
        $this->assertEquals([
            UserViewData::class => UserViewPolicyStub::class,
        ],
            $gate->policies());
    }

    public function testOwnerCanViewUsingAuthorizeData(): void
    {
        $gate = $this->getGate(new User(2));
        UserViewPolicyStub::defineSelf(UserViewData::class, $gate);
        UserViewAuthorizesDataStub::setGate($gate);
        $auth = new UserViewAuthorizesDataStub();
        $this->assertTrue($auth->check(
            GatePolicyType::VIEW->toMethodName(),
            new UserViewData(1, 2)
        ));
    }

    public function testOwnerCanViewUsingAuthorizeSelf(): void
    {
        $gate = $this->getGate(new User(2));
        $policy = new UserViewPolicyStub();
        $policy->defineSelf(UserViewData::class, $gate);
        UserViewAuthorizesDataStub::setGate($gate);
        UserViewData::setGate($gate);
        $data = new UserViewData(1, 2);
        $this->assertTrue($data->authorizeSelf());
    }

    public function testUserCannotUpdateUsingAuthorizeSelf(): void
    {
        $gate = $this->getGate(new User(2));
        $policy = new UserViewPolicyStub();
        $policy->defineSelf(UserViewData::class, $gate);
        UserViewAuthorizesDataStub::setGate($gate);
        UserViewData::setGate($gate);
        $data = new UserViewData(1, 2);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to update this record.');
        $data->authorizeSelf(GatePolicyType::UPDATE);
    }

    public function testUserCannotViewUsingAuthorizeData(): void
    {
        $gate = $this->getGate(new User(2));
        $policy = new UserViewPolicyStub();
        $policy->defineSelf(UserViewData::class, $gate);
        UserViewAuthorizesDataStub::setGate($gate);
        $auth = new UserViewAuthorizesDataStub();
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to view this record.');
        $auth->check(
            GatePolicyType::VIEW->toMethodName(),
            new UserViewData(1, 10)
        );
    }

    public function testUserCannotViewUsingAuthorizeSelf(): void
    {
        $gate = $this->getGate(new User(2));
        $policy = new UserViewPolicyStub();
        $policy->defineSelf(UserViewData::class, $gate);
        UserViewAuthorizesDataStub::setGate($gate);
        UserViewData::setGate($gate);
        $data = new UserViewData(1, 10);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to view this record.');
        $data->authorizeSelf(GatePolicyType::VIEW);
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container(), fn () => $user);
    }
}
