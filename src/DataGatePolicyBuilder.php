<?php

namespace Smorken\Data;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Foundation\Application;

class DataGatePolicyBuilder
{
    public function __construct(protected Gate $gate, protected Application $app)
    {
    }

    public static function getAbilityName(string $dataClass, string $ability): string
    {
        return $dataClass.'.'.$ability;
    }

    /**
     * @param  array<class-string, class-string>  $policies
     */
    public function create(array $policies): void
    {
        foreach ($policies as $dataClass => $policyClass) {
            $policyClass::defineSelf($dataClass, $this->gate);
        }
    }
}
