<?php

namespace Smorken\Data\Contracts;

use Smorken\Data\Constants\GatePolicyType;
use Spatie\LaravelData\Contracts\BaseData;

interface Data extends BaseData
{
    public function authorizeSelf(GatePolicyType $policyType = GatePolicyType::VIEW): bool;

    public function getAttributes(): array;

    public function getKey(): mixed;

    /**
     * @return string|array<string>
     */
    public function getKeyName(): string|array;

    public function hasKey(): bool;

    public function hasManyKeys(): bool;
}
