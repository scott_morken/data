<?php

namespace Smorken\Data\Contracts;

interface HasDataTransferObject
{
    public function getData();
}
