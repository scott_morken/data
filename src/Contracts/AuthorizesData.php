<?php

namespace Smorken\Data\Contracts;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Smorken\Data\Constants\GatePolicyType;

interface AuthorizesData
{
    public function authorizeData(?Data $data = null, ?GatePolicyType $policyType = null): bool;

    public function check(string $gateName, mixed $args = null): bool;

    public function checkRequest(Request $request, string $gateName, mixed $args = null): bool;

    public function checkUser(Authenticatable $user, string $gateName, mixed $args = null): bool;

    public function getGate(): ?Gate;

    public function hasGatePolicy(): bool;

    public function setThrowsAuthorizationException(bool $throw): void;

    public static function setGate(Gate $gate): void;
}
