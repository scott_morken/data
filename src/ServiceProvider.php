<?php

namespace Smorken\Data;

use Illuminate\Contracts\Auth\Access\Gate;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        Data::setGate($this->app[Gate::class]);
    }

    public function register(): void
    {
        $this->booting(function () {
            $this->registerGatePolicies();
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/gate-policies.php';
        $this->mergeConfigFrom($config, 'sm-gate-policies');
        $this->publishes([$config => config_path('sm-gate-policies.php')], 'config');
    }

    protected function registerGatePolicies(): void
    {
        $builder = new DataGatePolicyBuilder($this->app[Gate::class], $this->app);
        $builder->create($this->app['config']->get('sm-gate-policies', []));
    }
}
