<?php

namespace Smorken\Data\Concerns;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Smorken\Data\Constants\GatePolicyType;
use Smorken\Data\Contracts\Data;

/**
 * @property ?string $gateDataClass
 * @property ?\Smorken\Data\Constants\GatePolicyType $gatePolicyType
 */
trait AuthorizesData
{
    protected static ?Gate $gate = null;

    protected bool $throwsAuthorizationException = true;

    public static function setGate(Gate $gate): void
    {
        self::$gate = $gate;
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function authorizeData(?Data $data = null, ?GatePolicyType $policyType = null): bool
    {
        if ($this->getGate() && $this->hasGatePolicy()) {
            $ability = $this->getGateAbility($policyType);

            return $this->check($ability, $data);
        }

        return true;
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function check(string $gateName, mixed $args = null): bool
    {
        if ($this->throwsAuthorizationException) {
            return $this->getGate()->authorize($gateName, $args)->allowed();
        }

        return $this->getGate()->allows($gateName, $args);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function checkRequest(Request $request, string $gateName, mixed $args = null): bool
    {
        return $this->checkUser($request->user(), $gateName, $args);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function checkUser(Authenticatable $user, string $gateName, mixed $args = null): bool
    {
        $g = $this->getGate()->forUser($user);
        if ($this->throwsAuthorizationException) {
            return $g->authorize($gateName, $args)->allowed();
        }

        return $g->allows($gateName, $args);
    }

    public function getGate(): ?Gate
    {
        return self::$gate;
    }

    public function hasGatePolicy(): bool
    {
        $policies = $this->getGate()?->policies() ?? [];

        return $policies[$this->getGateDataClass() ?? static::class] ?? false;
    }

    public function setThrowsAuthorizationException(bool $throw): void
    {
        $this->throwsAuthorizationException = $throw;
    }

    protected function getGateAbility(?GatePolicyType $policyType): string
    {
        $type = $policyType ? $policyType->toMethodName() : $this->getGatePolicyType();

        return implode('.', array_filter([$this->getGateDataClass(), $type]));
    }

    protected function getGateDataClass(): ?string
    {
        return $this->getGateDataClassFromProperty();
    }

    protected function getGateDataClassFromProperty(): ?string
    {
        if (property_exists($this, 'gateDataClass')) {
            return $this->gateDataClass;
        }

        return null;
    }

    protected function getGateDataClassFromSelf(): ?string
    {
        return static::class;
    }

    protected function getGatePolicyType(): string
    {
        if (property_exists($this, 'gatePolicyType')) {
            return $this->gatePolicyType->toMethodName();
        }

        return GatePolicyType::VIEW->toMethodName();
    }
}
