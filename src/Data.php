<?php

namespace Smorken\Data;

use Illuminate\Support\Collection;
use Smorken\Data\Concerns\AuthorizesData;
use Smorken\Data\Constants\GatePolicyType;

abstract class Data extends \Spatie\LaravelData\Data implements Contracts\Data
{
    use AuthorizesData;

    protected string|array $keyName = 'id';

    public function authorizeSelf(GatePolicyType $policyType = GatePolicyType::VIEW): bool
    {
        return $this->authorizeData($this, $policyType);
    }

    public function getAttributes(): array
    {
        return $this->toArray();
    }

    public function getKey(): mixed
    {
        return $this->getPropertiesFromKeyName();
    }

    public function getKeyName(): string|array
    {
        return $this->keyName;
    }

    public function hasKey(): bool
    {
        $key = $this->getPropertiesFromKeyName();
        if (! is_array($key)) {
            return $key !== false;
        }

        return ! in_array(false, $key, true);
    }

    public function hasManyKeys(): bool
    {
        return is_array($this->getKeyName());
    }

    protected function getPropertiesFromKeyName(): mixed
    {
        $wantsOne = ! $this->hasManyKeys();
        $keyParts = new Collection((array) $this->getKeyName());
        $mapped = $keyParts->map(fn (string $property) => property_exists($this, $property) ? $this->$property : false);

        return $wantsOne ? $mapped->first() : $mapped->toArray();
    }
}
