<?php

namespace Smorken\Data;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;

abstract class DataGatePolicy implements Contracts\DataGatePolicy
{
    public static function defineSelf(string $dataClass, Gate $gate): void
    {
        $gate->policy($dataClass, static::class);
    }

    public function after(Authenticatable $user, string $ability): ?bool
    {
        return null;
    }

    public function before(Authenticatable $user, string $ability): ?bool
    {
        return null;
    }

    public function create(Authenticatable $user, ?array $attributes = null): bool|Response
    {
        return $this->isUserAllowedToCreate($user, $attributes);
    }

    public function delete(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->destroy($user, $data);
    }

    public function destroy(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->isUserAllowedToDestroy($user, $data);
    }

    public function forceDelete(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->destroy($user, $data);
    }

    public function index(Authenticatable $user): bool|Response
    {
        return $this->viewAny($user);
    }

    public function restore(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->destroy($user, $data);
    }

    public function update(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->isUserAllowedToUpdate($user, $data);
    }

    public function upsert(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        if ($data->getKey() === null) {
            return $this->isUserAllowedToCreate($user);
        }

        return $this->isUserAllowedToUpdate($user, $data);
    }

    public function view(Authenticatable $user, \Smorken\Data\Contracts\Data $data): bool|Response
    {
        return $this->isUserAllowedToView($user, $data);
    }

    public function viewAny(Authenticatable $user): bool|Response
    {
        return $this->isUserAllowedToViewAny($user);
    }

    /**
     * Create a new access response.
     */
    protected function allow(?string $message = null, mixed $code = null): Response
    {
        return Response::allow($message, $code);
    }

    /**
     * Throws an unauthorized exception.
     */
    protected function deny(?string $message = null, mixed $code = null): Response
    {
        return Response::deny($message, $code);
    }

    /**
     * Deny with a 404 HTTP status code.
     */
    protected function denyAsNotFound(?string $message = null, ?int $code = null): Response
    {
        return Response::denyWithStatus(404, $message, $code);
    }

    /**
     * Deny with an HTTP status code.
     */
    protected function denyWithStatus(int $status, ?string $message = null, ?int $code = null): Response
    {
        return Response::denyWithStatus($status, $message, $code);
    }

    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        return $this->deny('You do not have permission to create a new record.');
    }

    protected function isUserAllowedToDestroy(Authenticatable $user, \Smorken\Data\Contracts\Data $data): Response
    {
        return $this->deny('You do not have permission to delete this record.');
    }

    protected function isUserAllowedToUpdate(Authenticatable $user, \Smorken\Data\Contracts\Data $data): Response
    {
        return $this->deny('You do not have permission to update this record.');
    }

    protected function isUserAllowedToView(Authenticatable $user, \Smorken\Data\Contracts\Data $data): Response
    {
        return $this->deny('You do not have permission to view this record.');
    }

    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        return $this->deny('You do not have permission to view these records.');
    }
}
