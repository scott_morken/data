<?php

namespace Smorken\Data\Exceptions;

use Exception;

class DataTransferObjectMissing extends Exception
{
    public static function required(): self
    {
        return new static('A data transfer object is required.');
    }
}
